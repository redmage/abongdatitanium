var ANDROID_DPI_SCREEN_RATIO = 160;
var args = arguments[0] || {};
var screenWidth;

if (Ti.Platform.osname == "mobileweb") {
	screenWidth = Titanium.Platform.displayCaps.platformWidth;
}
else if (Ti.Platform.osname == "android") {
	screenWidth = Titanium.Platform.displayCaps.platformWidth / Ti.Platform.displayCaps.dpi * ANDROID_DPI_SCREEN_RATIO;
}

$.news_image.backgroundImage = args.news_image;
$.news_title.text = args.news_title;
$.news_title.width = screenWidth - 175;

function loadNewsDetail()
{
	var newsDetailWindow = Alloy.createController("news_detail", {
		news_detail_content: args.news_content
	}).getView();
	
	newsDetailWindow.open();
}