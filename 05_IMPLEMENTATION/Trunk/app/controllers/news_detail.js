var args = arguments[0] || {};

var html
	= "<!DOCTYPE html>"
	+ "<html>"
	+ "<head>"
        + "<meta charset='utf-8' />"
        + "<meta name='viewport' content='height=device-height, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, width=device-width' />"
    + "</head>"
    + "<body>";
html += args.news_detail_content;
html += "</body></html>";

$.news_detail_content.html = html;
$.news_detail_content.softKeyboardOnFocus = Titanium.UI.Android.SOFT_KEYBOARD_HIDE_ON_FOCUS;

function backToNews() {
	$.news_detail.close();
};