function Controller() {
    function backToNews() {
        $.news_detail.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "news_detail";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.news_detail = Ti.UI.createWindow({
        backgroundColor: "white",
        layout: "vertical",
        id: "news_detail"
    });
    $.__views.news_detail && $.addTopLevelView($.__views.news_detail);
    $.__views.__alloyId2 = Ti.UI.createView({
        backgroundColor: "#0281E4",
        height: 45,
        id: "__alloyId2"
    });
    $.__views.news_detail.add($.__views.__alloyId2);
    $.__views.news_detail_header_left_button = Ti.UI.createImageView({
        height: 25,
        left: 10,
        top: 10,
        width: 12,
        image: "/images/back-icon.png",
        id: "news_detail_header_left_button"
    });
    $.__views.__alloyId2.add($.__views.news_detail_header_left_button);
    backToNews ? $.__views.news_detail_header_left_button.addEventListener("click", backToNews) : __defers["$.__views.news_detail_header_left_button!click!backToNews"] = true;
    $.__views.__alloyId3 = Ti.UI.createLabel({
        color: "white",
        font: {
            fontSize: 17
        },
        top: 11,
        width: 60,
        text: "Tin tức",
        id: "__alloyId3"
    });
    $.__views.__alloyId2.add($.__views.__alloyId3);
    $.__views.news_detail_content = Ti.UI.createWebView({
        id: "news_detail_content"
    });
    $.__views.news_detail.add($.__views.news_detail_content);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    var html = "<!DOCTYPE html><html><head><meta charset='utf-8' /><meta name='viewport' content='height=device-height, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, width=device-width' /></head><body>";
    html += args.news_detail_content;
    html += "</body></html>";
    $.news_detail_content.html = html;
    $.news_detail_content.softKeyboardOnFocus = Titanium.UI.Android.SOFT_KEYBOARD_HIDE_ON_FOCUS;
    __defers["$.__views.news_detail_header_left_button!click!backToNews"] && $.__views.news_detail_header_left_button.addEventListener("click", backToNews);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;