function Controller() {
    function slide() {
        var animation1 = Titanium.UI.createAnimation();
        animation1.backgroundColor = "#0281E4";
        animation1.opacity = 1;
        var animation2 = Titanium.UI.createAnimation();
        animation2.backgroundColor = "white";
        animation2.opacity = .6;
        index >= news_image_slider.getViews().length && (index = 0);
        0 == index ? slider_page_control.children[news_image_slider.getViews().length - 1].animate(animation2) : slider_page_control.children[index - 1].animate(animation2);
        slider_page_control.children[index].animate(animation1);
        news_image_slider.scrollToView(index);
        index++;
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "news_image_slider";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.news_slider_place_holder = Ti.UI.createView({
        id: "news_slider_place_holder"
    });
    $.__views.news_slider_place_holder && $.addTopLevelView($.__views.news_slider_place_holder);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    var screenWidth;
    screenWidth = Titanium.Platform.displayCaps.platformWidth;
    var news_image_views = [];
    var slider_page_control = Ti.UI.createView({
        bottom: 10,
        height: 9,
        width: 252
    });
    var count = 0;
    _.each(args.news_content, function(item) {
        var news_title = Ti.UI.createLabel({
            color: "white",
            font: {
                fontSize: 18
            },
            height: 45,
            left: 10,
            text: item.Title,
            verticalAlign: Titanium.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
            width: screenWidth - 20,
            top: 10
        });
        var news_title_wrapper = Ti.UI.createView({
            backgroundColor: "black",
            bottom: 0,
            height: 80,
            opacity: .6,
            width: screenWidth
        });
        var view = Ti.UI.createView({
            backgroundImage: item.ImageLink
        });
        view.addEventListener("click", function() {
            var newsDetailWindow = Alloy.createController("news_detail", {
                news_detail_content: item.Content
            }).getView();
            newsDetailWindow.open();
        });
        news_title_wrapper.add(news_title);
        view.add(news_title_wrapper);
        news_image_views.push(view);
        var view;
        view = 0 == count ? Ti.UI.createView({
            backgroundColor: "#0281E4",
            opacity: 1,
            borderRadius: 20,
            height: 9,
            width: 9,
            left: 27 * count
        }) : Ti.UI.createView({
            backgroundColor: "white",
            opacity: .6,
            borderRadius: 20,
            height: 9,
            width: 9,
            left: 27 * count
        });
        count++;
        slider_page_control.add(view);
    });
    var news_image_slider = Ti.UI.createScrollableView({
        views: news_image_views
    });
    var index = 0;
    var timer = setInterval(slide, 1500);
    $.news_slider_place_holder.add(news_image_slider);
    $.news_slider_place_holder.add(slider_page_control);
    $.news_slider_place_holder.height = 3 * screenWidth / 4;
    $.news_slider_place_holder.width = screenWidth;
    news_image_slider.addEventListener("scrollend", function(e) {
        var animation1 = Titanium.UI.createAnimation();
        animation1.backgroundColor = "#0281E4";
        animation1.opacity = 1;
        var animation2 = Titanium.UI.createAnimation();
        animation2.backgroundColor = "white";
        animation2.opacity = .6;
        index >= news_image_slider.getViews().length && (index = 0);
        clearInterval(timer);
        slider_page_control.children[e.currentPage].animate(animation1);
        slider_page_control.children[index].animate(animation2);
        timer = setInterval(slide, 1500);
        index = e.currentPage;
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;