function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "indicator";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.indicator = Ti.UI.createWindow({
        backgroundColor: "transparent",
        layout: "vertical",
        height: 30,
        opacity: 1,
        width: 30,
        navBarHidden: true,
        id: "indicator"
    });
    $.__views.indicator && $.addTopLevelView($.__views.indicator);
    $.__views.activityIndicator = Ti.UI.createActivityIndicator({
        style: Titanium.UI.ActivityIndicatorStyle.BIG,
        id: "activityIndicator"
    });
    $.__views.indicator.add($.__views.activityIndicator);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.activityIndicator.show();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;