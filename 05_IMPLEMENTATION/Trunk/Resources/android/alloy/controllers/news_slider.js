function Controller() {
    function slide() {
        index >= news_count && (index = 0);
        0 == index ? slider_page_control_wrapper.children[news_count - 1].animate(previousAnimation) : slider_page_control_wrapper.children[index - 1].animate(previousAnimation);
        slider_page_control_wrapper.children[index].animate(nextAnimation);
        news_slider.scrollToView(index);
        index++;
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "news_slider";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.news_slider_place_holder = Ti.UI.createView({
        id: "news_slider_place_holder"
    });
    $.__views.news_slider_place_holder && $.addTopLevelView($.__views.news_slider_place_holder);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var ANDROID_DPI_SCREEN_RATIO = 160;
    var args = arguments[0] || {};
    var screenWidth;
    screenWidth = Titanium.Platform.displayCaps.platformWidth / Ti.Platform.displayCaps.dpi * ANDROID_DPI_SCREEN_RATIO;
    var index = 0;
    var news_slider_views = [];
    var slider_page_control_wrapper = Ti.UI.createView({
        bottom: 10,
        height: 9,
        width: 252
    });
    _.each(args.news_content, function(item) {
        var news_title = Ti.UI.createLabel({
            color: "white",
            font: {
                fontSize: 18
            },
            height: 45,
            left: 10,
            text: item.Title,
            top: 10,
            verticalAlign: Titanium.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
            width: screenWidth - 20
        });
        var news_title_wrapper = Ti.UI.createView({
            backgroundColor: "black",
            bottom: 0,
            height: 80,
            opacity: .6,
            width: screenWidth
        });
        var news_wrapper = Ti.UI.createView({
            backgroundImage: item.ImageLink
        });
        news_wrapper.addEventListener("click", function() {
            var newsDetailWindow = Alloy.createController("news_detail", {
                news_detail_content: item.Content
            }).getView();
            newsDetailWindow.open();
        });
        news_title_wrapper.add(news_title);
        news_wrapper.add(news_title_wrapper);
        news_slider_views.push(news_wrapper);
        var slider_page_control;
        slider_page_control = 0 == index ? Ti.UI.createView({
            backgroundColor: "#0281E4",
            borderRadius: 20,
            height: 9,
            left: 27 * index,
            opacity: 1,
            width: 9
        }) : Ti.UI.createView({
            backgroundColor: "white",
            borderRadius: 20,
            height: 9,
            left: 27 * index,
            opacity: .6,
            width: 9
        });
        slider_page_control_wrapper.add(slider_page_control);
        index++;
    });
    var news_slider = Ti.UI.createScrollableView({
        views: news_slider_views
    });
    index = 0;
    var news_count = news_slider.getViews().length;
    var nextAnimation = Titanium.UI.createAnimation();
    nextAnimation.backgroundColor = "#0281E4";
    nextAnimation.opacity = 1;
    var previousAnimation = Titanium.UI.createAnimation();
    previousAnimation.backgroundColor = "white";
    previousAnimation.opacity = .6;
    var timer = setInterval(slide, 1500);
    news_slider.addEventListener("scrollend", function(e) {
        index >= news_count && (index = 0);
        clearInterval(timer);
        slider_page_control_wrapper.children[e.currentPage].animate(nextAnimation);
        slider_page_control_wrapper.children[index].animate(previousAnimation);
        timer = setInterval(slide, 1500);
        index = e.currentPage;
    });
    $.news_slider_place_holder.add(news_slider);
    $.news_slider_place_holder.add(slider_page_control_wrapper);
    $.news_slider_place_holder.height = 3 * screenWidth / 4;
    $.news_slider_place_holder.width = screenWidth;
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;