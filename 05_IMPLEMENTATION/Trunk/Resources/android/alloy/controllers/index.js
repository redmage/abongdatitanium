function Controller() {
    function loadNews() {
        indicatorWindow.open();
        var url = SERVICE_URL + "GetNews?imei=354661058173460";
        var xhr = Ti.Network.createHTTPClient({
            onload: function() {
                news_content = JSON.parse(this.responseText);
                $.news_content.removeAllChildren();
                setNewsContent();
                indicatorWindow.close();
            },
            onerror: function() {
                alert("Không thể kết nối đến ABongDa! Xin vui lòng thử lại sau!");
                indicatorWindow.close();
            },
            timeout: TIMEOUT
        });
        xhr.open("GET", url);
        xhr.send();
    }
    function setNewsContent() {
        var scrollView = Ti.UI.createScrollView({
            layout: "vertical",
            scrollType: "vertical"
        });
        scrollView.add(Alloy.createController("news_slider", {
            news_content: news_content
        }).getView());
        _.each(news_content, function(item) {
            scrollView.add(Alloy.createController("news_row", {
                news_content: item.Content,
                news_image: item.ImageLink,
                news_title: item.Title
            }).getView());
        });
        $.news_content.add(scrollView);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.index = Ti.UI.createWindow({
        backgroundColor: "white",
        layout: "vertical",
        exitOnClose: true,
        navBarHidden: true,
        id: "index"
    });
    $.__views.index && $.addTopLevelView($.__views.index);
    loadNews ? $.__views.index.addEventListener("open", loadNews) : __defers["$.__views.index!open!loadNews"] = true;
    $.__views.__alloyId0 = Ti.UI.createView({
        backgroundColor: "#0281E4",
        height: 45,
        id: "__alloyId0"
    });
    $.__views.index.add($.__views.__alloyId0);
    $.__views.__alloyId1 = Ti.UI.createLabel({
        color: "white",
        font: {
            fontSize: 17
        },
        top: 11,
        width: 60,
        text: "Tin tức",
        id: "__alloyId1"
    });
    $.__views.__alloyId0.add($.__views.__alloyId1);
    $.__views.news_header_right_button = Ti.UI.createImageView({
        height: 25,
        right: 10,
        top: 10,
        width: 25,
        image: "/images/refresh-icon@2x.png",
        id: "news_header_right_button"
    });
    $.__views.__alloyId0.add($.__views.news_header_right_button);
    loadNews ? $.__views.news_header_right_button.addEventListener("click", loadNews) : __defers["$.__views.news_header_right_button!click!loadNews"] = true;
    $.__views.news_content = Ti.UI.createView({
        id: "news_content"
    });
    $.__views.index.add($.__views.news_content);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var ANDROID_DPI_SCREEN_RATIO = 160;
    var SERVICE_URL = "http://api.bongda.bz:4404/AjaxService.svc/";
    var TIMEOUT = 1e4;
    var indicatorWindow = Alloy.createController("indicator").getView();
    var news_content = [];
    screenWidth = Titanium.Platform.displayCaps.platformWidth / Ti.Platform.displayCaps.dpi * ANDROID_DPI_SCREEN_RATIO;
    $.index.open();
    __defers["$.__views.index!open!loadNews"] && $.__views.index.addEventListener("open", loadNews);
    __defers["$.__views.news_header_right_button!click!loadNews"] && $.__views.news_header_right_button.addEventListener("click", loadNews);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;