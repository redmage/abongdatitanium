function Controller() {
    function loadNewsDetail() {
        var newsDetailWindow = Alloy.createController("news_detail", {
            news_detail_content: args.news_content
        }).getView();
        newsDetailWindow.open();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "news_row";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.news_row = Ti.UI.createView({
        backgroundColor: "#EBE9E9",
        height: 101,
        id: "news_row"
    });
    $.__views.news_row && $.addTopLevelView($.__views.news_row);
    loadNewsDetail ? $.__views.news_row.addEventListener("click", loadNewsDetail) : __defers["$.__views.news_row!click!loadNewsDetail"] = true;
    $.__views.news_image = Ti.UI.createView({
        borderColor: "white",
        borderWidth: 5,
        height: 90,
        left: 5,
        top: 5,
        width: 120,
        id: "news_image"
    });
    $.__views.news_row.add($.__views.news_image);
    $.__views.news_title = Ti.UI.createLabel({
        color: "#2F3E46",
        left: 140,
        right: 10,
        id: "news_title"
    });
    $.__views.news_row.add($.__views.news_title);
    $.__views.__alloyId4 = Ti.UI.createImageView({
        height: 15,
        image: "/images/nav-arrow.png",
        right: 15,
        width: 10,
        id: "__alloyId4"
    });
    $.__views.news_row.add($.__views.__alloyId4);
    $.__views.__alloyId5 = Ti.UI.createView({
        backgroundColor: "white",
        bottom: 0,
        height: 1,
        id: "__alloyId5"
    });
    $.__views.news_row.add($.__views.__alloyId5);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var ANDROID_DPI_SCREEN_RATIO = 160;
    var args = arguments[0] || {};
    var screenWidth;
    screenWidth = Titanium.Platform.displayCaps.platformWidth / Ti.Platform.displayCaps.dpi * ANDROID_DPI_SCREEN_RATIO;
    $.news_image.backgroundImage = args.news_image;
    $.news_title.text = args.news_title;
    $.news_title.width = screenWidth - 175;
    __defers["$.__views.news_row!click!loadNewsDetail"] && $.__views.news_row.addEventListener("click", loadNewsDetail);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;